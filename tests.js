test("VALID PLATES", function (assert) {
  assert.equal(isValidPlate("9999BCD"), true, "VALID PLATE STANDARD");
  assert.equal(isValidPlate("5239JHF"), true, "VALID PLATE WITH DIFFERENT NUMBERS AND LETTERS");
  assert.equal(isValidPlate("1139dkj"), true, "VALID PLATE WITH LOWERCASE LETTERS");
});

test("INVALID PLATES", function (assert) {
  assert.equal(isValidPlate("523DJ3H"), false, "INVALID PLATE WITH NUMBERS BETWEEN LETTERS");
  assert.equal(isValidPlate("5239"), false, "INVALID PLATE ONLY WITH 4 NUMBERS");
  assert.equal(isValidPlate("5239D"), false, "INVALID PLATE ONLY WITH 4 NUMBERS AND A UPPERCASE");
  assert.equal(isValidPlate("5239wx"), false, "INVALID PLATE ONLY WITH 4 NUMBERS AND 2 LOWERCASE");
  assert.equal(isValidPlate("5239Ñññ"), false, "INVALID PLATE WITH Ñ and ñ");
  assert.equal(isValidPlate("5239Qqq"), false, "INVALID PLATE WITH Q and q");
  assert.equal(isValidPlate("5239AAA"), false, "INVALID PLATE WITH VOWELS");
  assert.equal(isValidPlate("asddjfj"), false, "INVALID PLATE ONLY WITH LETTERS");
  assert.equal(isValidPlate("1231545"), false, "INVALID PLATE ONLY WITH NUMBERS");
  assert.equal(isValidPlate("1139dkjdddddd"), true, "INVALID PLATE THAT TAKES AS VALID ENTERING MORE CHARACTER THAN NEEDED");
  assert.equal(isValidPlate("1139dkjjlsdjk"), true, "INVALID PLATE THAT TAKES AS VALID ENTERING MORE CHARACTER THAN NEEDED 2");
});
